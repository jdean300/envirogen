﻿using System.Windows.Controls;

namespace EnviroGenDisplay.Views
{
    /// <summary>
    /// Interaction logic for HydraulicErosionView.xaml
    /// </summary>
    public partial class HydraulicErosionView : UserControl
    {
        public HydraulicErosionView()
        {
            InitializeComponent();
        }
    }
}
