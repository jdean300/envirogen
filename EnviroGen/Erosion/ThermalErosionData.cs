﻿namespace EnviroGen.Erosion
{
    public class ThermalErosionData : ErosionData
    {
        public float TalusAngle { get; set; }
    }
}
